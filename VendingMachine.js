class VendingMachine {

    constructor(item, coin) {
        this.item = item;
        this.coin = coin;
    }

    vend(itemcode = null, coin = this.coin) {

        if (itemcode == null) {
            return "Item code is Null !";
            return false;
        }

        if (typeof coin != "number") {
            return "Coin input accept Number only !";
            return false;
        }

        let itemdetail = this.item.find((item) => item.code == itemcode.toUpperCase());

        if (typeof itemdetail == "undefined") {
            return "Invalid selection! : Money in vending machine = " + coin.toFixed(2);
            return false;
        } else {
            let ItemSelected = {
                itemName: itemdetail.name,
                itemPrice: itemdetail.price,
                itemCode: itemdetail.code,
                itemQty: itemdetail.qty
            };
            let {
                itemName,
                itemPrice,
                itemCode,
                itemQty
            } = ItemSelected;

            if (itemPrice > coin) {
                return "Not enough money!";
                return false;
            }
            else if (itemQty == 0) {
                return itemName + " : Out of stock!";
                return false;
            }
            else {
                var ReturnText = "Vending " + itemName;
                var ChangeText = "";
                if (coin > itemPrice) {
                    let change = coin - itemPrice;
                    ChangeText = " with " + change.toFixed(2) + " change";
                }

                itemdetail.qty = itemQty - 1;
                this.coin = this.coin - itemPrice;
                this.coin = this.coin.toFixed(2)
                this.coin = parseFloat(this.coin);
                return ReturnText + ChangeText;
            }
        }
    }
}



let itemlist = [{
        name: "Smarties",
        code: "A01",
        qty: 2,
        price: 0.6
    },
    {
        name: "Caramac Bar",
        code: "A02",
        qty: 4,
        price: 0.7
    },
    {
        name: "Dairy Milk",
        code: "A03",
        qty: 6,
        price: 0.65
    },
    {
        name: "Freddo",
        code: "A04",
        qty: 1,
        price: 10
    },
    {
        name: "Oreo",
        code: "A05",
        qty: 0,
        price: 0.45
    },
    {
        name: "KitKat",
        code: "A06",
        qty: 3,
        price: 0.3
    },
    {
        name: "Tokyo Banana",
        code: "A07",
        qty: 2,
        price: 0.7
    },
] ;


let CoinInput = 2.10;
let Vending = new VendingMachine(itemlist, CoinInput);
let vend1 = Vending.vend("A07", Vending.coin);
let vend2 = Vending.vend("A07", Vending.coin);
let vend3 = Vending.vend("A07", Vending.coin);
let vend4 = Vending.vend("A09", Vending.coin);
let vend5 = Vending.vend("A02", Vending.coin);
let vend6 = Vending.vend("A02", Vending.coin);
let vend7 = Vending.vend("A02", 'coin');
let vend8 = Vending.vend();

console.log('Coin insert = ' + CoinInput.toFixed(2));
console.log(vend1);
console.log(vend2);
console.log(vend3);
console.log(vend4);
console.log(vend5);
console.log(vend6);
console.log('Coin balance = ' + Vending.coin.toFixed(2));
console.log(vend7);
console.log(vend8);
